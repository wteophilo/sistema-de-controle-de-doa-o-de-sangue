package br.com.scdsp.modulousuario.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

public interface UsuarioRepository  extends CrudRepository<Usuario, Long>{
	
	Usuario findOne( Long id);

}
