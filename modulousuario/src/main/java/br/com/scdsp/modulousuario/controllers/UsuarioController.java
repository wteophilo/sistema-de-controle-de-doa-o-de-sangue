package br.com.scdsp.modulousuario.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.scdsp.modulousuario.models.Usuario;
import br.com.scdsp.modulousuario.models.UsuarioRepository;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepository repository;

		
	@RequestMapping(value = "/lista", method = RequestMethod.GET,  produces = "application/json")
	public List<Usuario> getAllUsuarioInJon() {
		List<Usuario> usuarios = (List<Usuario>) repository.findAll();
		return usuarios;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Usuario getUsuarioInJon(@PathVariable Long id) {
		Usuario usuario = repository.findOne(id);
		return usuario;
	}

	@RequestMapping
	public String index() {
		return "usuario/index";
	}
	
	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastro(Usuario usuario) {
		return "usuario/index";
	}
	
}
